#!/bin/bash

# Example URL: myurl="https://mywebdav.url/immunogenomics/RUNS/run246/results-tbcell/"
myurl=$1; shift

myfiles="*-all_info.csv *-clones-subs.csv"
filelist=`echo $myfiles | perl -ne "@c=split(/\s/); print join(',', @c);"`

starttime=`date +%s`

echo "Calculating checksums..."
shasum $myfiles > CHECKSUM.$starttime
wait

echo "Transferring files..."
curl -T "{$filelist,CHECKSUM.$starttime}" --netrc $myurl
wait

endtime=`date +%s`
difftime=`expr ${endtime} - ${starttime}`

echo "Finished in $difftime seconds"
