'''
RESEDA - REPertoire SEquencing Data Analysis
Copyright (C) 2016 Barbera DC van Schaik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import sys

# Create this file with: wc -l *-all_info.csv > wc-run04.txt
wcFile = sys.argv[1]   # "wc.txt"

try:
    fh = open(wcFile, "r")
except:
    sys.exit("cannot open wcFile")

# Store wc in this dictionary structure: d[(sample, celltype)][mid] = wc

d = dict()
for line in fh:
    line = line.strip()  # remove whitespaces before and after line
    (wc, filepath) = line.split() # get wordcount and filepath
    wc = int(wc)
    path = filepath.split("/")    # get directories and filename

    filename = path[-1]
    if filename == "total":
        continue

    elements = filename.split("-") # filename contains info about:
    celltype = elements[-2]
    mid = elements[-3]
    sample = "-".join(elements[:-3])

    if mid != "nomatch":
        d[(sample,celltype)] = d.get((sample,celltype), dict()) # create new dict if not exist
        d[(sample,celltype)][mid] = wc

for (sample,celltype) in sorted(d):
    d_tmp = d[(sample, celltype)]
    topmid = sorted(d_tmp, key=d_tmp.get, reverse=True)[0]
    #print("Highest MID:", sample, celltype, topmid, d[(sample,celltype)][topmid])
    print("mv", sample+"-"+topmid+"*", "correct-mid/")
