'''
RESEDA - REPertoire SEquencing Data Analysis
Copyright (C) 2016 Barbera DC van Schaik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import sys
from Bio import SeqIO

usage = "Usage: " + sys.argv[0] + " IMGT protein sequences with IMGT gaps"

if len(sys.argv) < 2:
    sys.exit(usage)

for myfile in sys.argv[1:]:
    try:
        fhIn = open(myfile, "r")
    except:
        sys.exit("cannot open file " + myfile)
    try:
        fhOut = open(myfile+".csv", "w")
    except:
        sys.exit("cannot create file " + myfile+".csv")

    print("V.gene,func,seq", file=fhOut)
    for record in SeqIO.parse(fhIn, "fasta") :
        c = record.description.split("|")
        (name,allele) = c[1].split("*")
        print(",".join([name,c[3],str(record.seq)]), file=fhOut)
