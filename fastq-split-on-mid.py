'''
RESEDA - REPertoire SEquencing Data Analysis
Copyright (C) 2016 Barbera DC van Schaik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import sys
import os
import gzip
import regex
from sequences import *
from Bio import SeqIO

# Check if arguments were given to this script
if len(sys.argv) < 4:
    sys.exit('Usage: %s midfile outdir fastq-file(s)' % sys.argv[0])

[midFile, outdir] = sys.argv[1:3]
fastqFiles = sys.argv[3:]

### Functions ###

def sortMIDS (motifs, fastqFile, outdir):
    '''
    In: motifs (list), fastqFile (file path), outdir (directory path without slash at the end)
    Out: output files are generated in the directory specified in "outdir"
    '''

    prefixFastq = os.path.basename(fastqFile)
    prefixFastq = prefixFastq.replace(".fastq.gz","")

    print("Processing", prefixFastq)

    # Open fastq2, check the MIDs and create a barcode file
    try:
        fhIn = gzip.open(fastqFile, "rb")
    except:
        sys.exit("cannot open file:" + fastqFile)

    # Go through fastq entries of fastq file
    fh = dict()
    fh["nomatch"] = gzip.open(outdir + "/" + prefixFastq + "-nomatch.fastq.gz", "w")
    fh["report"] = open(outdir + "/" + prefixFastq + "-report.txt", "w")
    fh["midcount"] = open(outdir + "/" + prefixFastq + "-midcount.txt", "w")
    midCount = dict()
    for record in SeqIO.parse(fhIn, "fastq") :

        # Check which MID is present (4N MID 5nt)
        sequence = str(record.seq).upper()
        keepMatch = 0
        for motif in motifs:
            # Search for the motif in normal orientation
            m = regex.search(motif, sequence)
            if not m:
            # Search for the motif in reverse complement if it was not found
                m = regex.search(motif, comrev(sequence))
                if not m:
                    next
                else:
                    keepMatch = m
            else:
                keepMatch = m
        
        # Write entry to the file with this particular MID
        # If there is no MID in the sequence write it to the file "nomatch"
        
        if keepMatch == 0:
            print(record.id, "nomatch", "nomatch", "nomatch", file=fh["report"])
            SeqIO.write(record, fh["nomatch"], "fastq")
            midCount["nomatch"] = midCount.get("nomatch", 0) + 1
        else:
            mid = keepMatch.group(2)
            if mid not in fh:  # If it is a new MID open a new output file
                fh[mid] = gzip.open(outdir + "/" + prefixFastq + "-" + mid + ".fastq.gz", "w")
            print(record.id, keepMatch.group(1), keepMatch.group(2), keepMatch.group(3), file=fh["report"])
            SeqIO.write(record, fh[mid], "fastq")
            midCount[mid] = midCount.get(mid, 0) + 1

    # Write mid counts to file
    for mid,freq in midCount.items():
        print(mid, freq, file=fh["midcount"])

    # Close all files
    for mykey in fh:
        fh[mykey].close()

### Main ###


# Read file with MIDs
motifs = readMotifsFromFile(midFile)

# Create output directory if it doesn't exist
if not os.path.exists(outdir):
    os.makedirs(outdir)

# Sort the fastq files on MID
for i in range(len(fastqFiles)):
    sortMIDS(motifs, fastqFiles[i], outdir)

print("FINISHED")
