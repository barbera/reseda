'''
RESEDA - REPertoire SEquencing Data Analysis
Copyright (C) 2016 Barbera DC van Schaik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from __future__ import print_function
import sys

# Input
if len(sys.argv) < 2:
    sys.exit("Usage: report-after-v-reassignment.py /path/to/*.rr.clones_subs.csv")
datafiles = sys.argv[1:]

def countReads (datafile):
    try:
        fh = open(datafile)
    except:
        sys.exit("cannot open file " + datafile)

    header = fh.readline()
    totalreads = 0
    for line in fh:
        line = line.strip()
        c = line.split()
        freq = int(c[3])
        totalreads += freq

    fh.close()

    return(totalreads)

try:
    fhOut = open("report-AFTER-V-REASSIGNMENT.txt", "w")
except:
    sys.exit("cannot write to file")

for datafile in datafiles:
    totalreads = countReads(datafile)
    print(datafile, totalreads)
    print(datafile, totalreads, file=fhOut)

fhOut.close()
